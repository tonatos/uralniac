# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('article', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='articleitem',
            name='img',
        ),
        migrations.AddField(
            model_name='articleitem',
            name='meta_description',
            field=models.CharField(max_length=255, null=True, verbose_name='\u0421\u0415\u041e \u041e\u043f\u0438\u0441\u0430\u043d\u0438\u0435', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='articleitem',
            name='meta_title',
            field=models.CharField(max_length=255, null=True, verbose_name='\u0421\u0415\u041e \u0417\u0430\u0433\u043e\u043b\u043e\u0432\u043e\u043a', blank=True),
            preserve_default=True,
        ),
    ]
