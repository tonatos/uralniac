#-*- coding: UTF-8 -*-
from cms.app_base import CMSApp
from cms.apphook_pool import apphook_pool

from .menu import ArticleMenu

class ArticleApphook(CMSApp):
    name = u'Статьи'
    urls = ['article.urls']
    menus = [ArticleMenu]

apphook_pool.register(ArticleApphook)