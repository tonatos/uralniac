#-*- coding: UTF-8 -*-
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView

from .models import *
from uralniac.views import SetMeta


class ArticleItemListView(ListView):
    model = ArticleItem
    context_object_name='items'
    template_name = 'article/list.html'

    def get_queryset(self):
        return super(ArticleItemListView, self).get_queryset().order_by('-pk')


class ArticleItemDetailView(DetailView, SetMeta):
    model = ArticleItem
    context_object_name='item'
    template_name = 'article/item.html'