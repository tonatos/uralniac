#-*- coding: UTF-8 -*-
from cms.menu_bases import CMSAttachMenu
from menus.base import NavigationNode
from menus.menu_pool import menu_pool

from .models import ArticleItem

class ArticleMenu(CMSAttachMenu):
    name = u'Меню статей'

    def get_nodes(self, request):
        nodes = []

        for item in ArticleItem.objects.all():
            nodes.append(NavigationNode(item.title,
                                        item.get_absolute_url(),
                                        item.pk,
                                        visible=False))

        return nodes

menu_pool.register_menu(ArticleMenu) # register the menu