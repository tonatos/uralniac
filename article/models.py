#-*- coding: UTF-8 -*-

from django.db import models
from djangocms_text_ckeditor.fields import HTMLField
from filer.fields.image import FilerImageField
from filer.fields.file import FilerFileField

from uralniac.models import MetaFields

class ArticleItem(MetaFields):
    title = models.CharField(u'Название статьи', max_length=150)

    # consider if you need only one image or multiple images
    #img =  FilerImageField(verbose_name=u'Изображение', null=True, blank=True,
    #                       related_name="article_img")

    content = HTMLField(u'Содержимое', blank=True, configuration='CKEDITOR_HTML_SETTINGS')

    order = models.IntegerField(u'Подрядок сортировки', default=0, blank=True)

    @models.permalink
    def get_absolute_url(self):
        return 'article_item', [self.id]

    def __unicode__(self):
        return self.title

    class Meta:
        verbose_name = u'Статья'
        verbose_name_plural = u'Статьи'
