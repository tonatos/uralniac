#-*- coding: UTF-8 -*-

from django.db import models
from djangocms_text_ckeditor.fields import HTMLField
from filer.fields.image import FilerImageField
from filer.fields.file import FilerFileField

from uralniac.models import MetaFields

# Create your models here.
class Project(MetaFields):
    title = models.CharField(u'Название проекта', max_length=255)
    content = HTMLField(u'Описание', max_length=255, null=True, default='', blank=True)
    img =  FilerImageField(verbose_name=u'Изображение', null=True, blank=True,
                           related_name="company_logo")
    class Meta:
        verbose_name = u'Проект'
        verbose_name_plural = u'Проекты'

    @models.permalink
    def get_absolute_url(self):
        return 'project_item', [self.id]