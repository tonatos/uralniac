from django.conf.urls import patterns, url

from .models import *
from .views import *

urlpatterns = patterns('',
    url(r'^$', ProjectListView.as_view()),

    url(r'^(?P<pk>\d+)/$', ProjectItemDetailView.as_view(),
        name='project_item')
)
