var showModal = function() {
    $('body').css({
       'overflow': 'hidden'
   });
   $('.overlay').show();
   $('.modal').css({
       'margin-top': 200
   });
   return false;
}
var copyToClipboard = function(text) {
    if (window.clipboardData) { // Internet Explorer
        window.clipboardData.setData("Text", text);
    } else {
        unsafeWindow.netscape.security.PrivilegeManager.enablePrivilege("UniversalXPConnect");
        const clipboardHelper = Components.classes["@mozilla.org/widget/clipboardhelper;1"].getService(Components.interfaces.nsIClipboardHelper);
        clipboardHelper.copyString(text);
    }
}


var pageUrl = location.pathname;
$(document).ready(function(){
    var clone = null;
    $('.direction-block .direction-item').hover(function(){
        self = $(this);

        if (!self.hasClass('hover')) {
            $('.direction-item.hover').remove();
            clone = self.clone().addClass('hover');
            clone.find('.text').text( self.data('content') );
            self.before( clone );
        }
    }, function() {});

    $('.direction-block').hover(function(){}, function() {
        $('.direction-item.hover').remove();
    });


    // Попповер
    $('.overlay').ready(function(){
        //$('.overlay').height( $(document).height() );
    });
    var closePopover = function() {
       window.history.back();
       $('.overlay').hide();
       $('body').css({
           'overflow': 'auto'
       });
    }
    $('.modal .close a').click(function(){
        closePopover();
        return false;
    });
    $(document).keydown(function(e){
        if (e.keyCode == 27) {
            closePopover();
        }
    });

    $('#copyToClipboard').click(function(e){
        e.preventDefault();
    }).clipboard({
        path: '/static/js/jquery.clipboard.swf',

        copy: function() {
            var this_sel = $(this);

            // Return text in closest element (useful when you have multiple boxes that can be copied)
            return $('#' + this_sel.data('target')).text();
        }
    });


    $('.ajax-news-item').click(function(){
        var $link = $(this)
        $.get( $(this).attr('href'), {}, function(data){
            history.pushState(null, '', $link.attr('href'));
            showModal();
            $('#modal-data').html(data);
        });

       return false;
    })
})