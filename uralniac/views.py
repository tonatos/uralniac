# -*- coding: utf-8 -*-
from django.shortcuts import render
from django.conf import settings
from django.contrib import messages
from django.core.mail import send_mail
from django.template.loader import render_to_string
from django.core.urlresolvers import reverse
from django.contrib.auth.models import User
from django.views.generic.base import ContextMixin

from .models import FeedbackSubject
from .forms import FeedbackSubjectForm


def page(request, page=None):
    page = page if page is not None else 'index'
    return render(request, "%s.html" % page)

def feedback_form(request):
    if request.method == 'POST':
        feedback_form = FeedbackSubjectForm(data = request.POST)

        if feedback_form.is_valid():
            ask = feedback_form.save(commit = False)
            ask.save()

            try:
                email = User.objects.filter(is_superuser = True)[0].email
            except ValueError:
                email = settings.MAIN_EMAIL

            send_mail(
                u'Сообщение с сайта «Научно-инжиниринговый центр архитектуры и строительства»',
                render_to_string(
                    'email/email_feedback_form.txt',
                    {
                        'ask': ask,
                        'url': request.build_absolute_uri(
                            reverse('admin:uralniac_feedbacksubject_change', args=[ask.pk])
                        )
                    }
                ), email, [ email ]
            )
            messages.success(
                request,
                'Сообщение было успешно отправлено. Спасибо за проявленное внимание!'
            )
            return render(request, 'index/feedback_form.html', {'form': FeedbackSubjectForm()})
    else:
        feedback_form = FeedbackSubjectForm()

    return render(request, 'index/feedback_form.html', {'form': feedback_form})


class SetMeta(ContextMixin):
    def get_context_data(self, *args, **kwargs):
        context = super(SetMeta, self).get_context_data(*args, **kwargs)
        tags = {
            'meta_title': '',
            'meta_description': '',
        }

        for i in tags.keys():
            try:
                tag = getattr(self.object, 'wqd')
                if tag:
                    context.update({i: tag})
            except AttributeError:
                pass

        return context
