#-*- coding: UTF-8 -*-
from django import template

from article.models import *
from news.models import *
from directions.models import *
from uralniac.forms import FeedbackSubjectForm

register = template.Library()

@register.inclusion_tag('index/directions.html')
def get_directions():
    return {'items': DirectionsItem.objects.filter()[:8] }

@register.inclusion_tag('index/solution.html')
def get_solution():
    return {'items': SolutionItem.objects.filter()[:4] }

@register.inclusion_tag('index/news.html')
def get_news():
    return {'items': NewsItem.objects.filter()[:3] }

@register.inclusion_tag('index/articles.html')
def get_articles():
    return {'items': ArticleItem.objects.filter()[:4] }


@register.inclusion_tag('index/feedback_form.html')
def feedback_form(request):
    print 1
    return {'form': FeedbackSubjectForm(), 'request': request }