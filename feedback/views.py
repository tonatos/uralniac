#-*- coding: UTF-8 -*-
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView

from .models import *
from uralniac.views import SetMeta


class FeedbackItemListView(ListView):
    model = FeedbackItem
    context_object_name='items'
    template_name = 'feedback/list.html'

    def get_queryset(self):
        return super(FeedbackItemListView, self).get_queryset().order_by('order')


class FeedbackItemDetailView(DetailView, SetMeta):
    model = FeedbackItem
    context_object_name='item'
    template_name = 'feedback/item.html'