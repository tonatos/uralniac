#-*- coding: UTF-8 -*-
from cms.menu_bases import CMSAttachMenu
from menus.base import NavigationNode
from menus.menu_pool import menu_pool

from .models import FeedbackItem

class FeedbackMenu(CMSAttachMenu):
    name = u'Меню отзывов'

    def get_nodes(self, request):
        nodes = []

        for item in FeedbackItem.objects.all():
            nodes.append(NavigationNode(item.title,
                                        item.get_absolute_url(),
                                        item.pk))

        return nodes

menu_pool.register_menu(FeedbackMenu) # register the menu