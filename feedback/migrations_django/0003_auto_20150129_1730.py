# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import djangocms_text_ckeditor.fields


class Migration(migrations.Migration):

    dependencies = [
        ('feedback', '0002_auto_20150123_0355'),
    ]

    operations = [
        migrations.AddField(
            model_name='feedbackitem',
            name='meta_description',
            field=models.CharField(max_length=255, null=True, verbose_name='\u0421\u0415\u041e \u041e\u043f\u0438\u0441\u0430\u043d\u0438\u0435', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='feedbackitem',
            name='meta_title',
            field=models.CharField(max_length=255, null=True, verbose_name='\u0421\u0415\u041e \u0417\u0430\u0433\u043e\u043b\u043e\u0432\u043e\u043a', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='feedbackitem',
            name='description',
            field=djangocms_text_ckeditor.fields.HTMLField(max_length=2000, verbose_name='\u0421\u043e\u0434\u0435\u0440\u0436\u0438\u043c\u043e\u0435 \u043e\u0442\u0437\u044b\u0432\u0430', blank=True),
            preserve_default=True,
        ),
    ]
