#-*- coding: UTF-8 -*-
from django.contrib import admin
from django.contrib.admin.options import TabularInline, StackedInline

from .models import *

class DirectionsItemAdmin(admin.ModelAdmin):
    ordering = ['order',]
    list_display = ('title', 'order',)

class SolutionItemAdmin(admin.ModelAdmin):
    ordering = ['order',]
    list_display = ('title', 'order',)

admin.site.register(DirectionsItem, DirectionsItemAdmin)
admin.site.register(SolutionItem, SolutionItemAdmin)