#-*- coding: UTF-8 -*-

from django.db import models
from djangocms_text_ckeditor.fields import HTMLField
from filer.fields.image import FilerImageField
from filer.fields.file import FilerFileField

from uralniac.models import MetaFields

class DirectionsItem(MetaFields):
    title = models.CharField(u'Заголовок', max_length=100)

    # consider if you need only one image or multiple images
    img =  FilerImageField(verbose_name=u'Изображение', null=True, blank=True,
                           related_name="direction_img")

    description = models.TextField(u'Описание', blank=True)
    content = HTMLField(u'Содержимое', blank=True)

    order = models.IntegerField(u'Порядок сортировки', default=0, blank=True)

    @models.permalink
    def get_absolute_url(self):
        return 'directions_item', [self.id]

    def __unicode__(self):
        return self.title

    class Meta:
        verbose_name = u'Направление детятельности'
        verbose_name_plural = u'Направления деятельности'

class SolutionItem(MetaFields):
    title = models.CharField(u'Заголовок', max_length=100)

    # consider if you need only one image or multiple images
    img =  FilerImageField(verbose_name=u'Изображение', null=True, blank=True,
                           related_name="solution_img")

    description = models.TextField(u'Описание', blank=True)
    content = HTMLField(u'Содержимое', blank=True)

    order = models.IntegerField(u'Порядок сортировки', default=0, blank=True)

    @models.permalink
    def get_absolute_url(self):
        return 'solution_item', [self.id]

    def __unicode__(self):
        return self.title

    class Meta:
        verbose_name = u'Отраслевое решение'
        verbose_name_plural = u'Отраслевые решения'
