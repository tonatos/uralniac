#-*- coding: UTF-8 -*-
from cms.menu_bases import CMSAttachMenu
from menus.base import NavigationNode
from menus.menu_pool import menu_pool

from .models import CertificateItem

class CertificateMenu(CMSAttachMenu):
    name = u'Меню сертификатов'

    def get_nodes(self, request):
        nodes = []
        return nodes

menu_pool.register_menu(CertificateMenu) # register the menu