from django.conf.urls import patterns, url

from .models import *
from .views import *

urlpatterns = patterns('',
    url(r'^$', CertificateItemListView.as_view()),

    url(r'^(?P<pk>\d+)/$', CertificateItemDetailView.as_view(),
        name='certificate_item')
)
