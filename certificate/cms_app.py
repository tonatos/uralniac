#-*- coding: UTF-8 -*-
from cms.app_base import CMSApp
from cms.apphook_pool import apphook_pool

from .menu import CertificateMenu

class CertificateApphook(CMSApp):
    name = u'Сертификаты'
    urls = ['certificate.urls']
    menus = [CertificateMenu]

apphook_pool.register(CertificateApphook)